package main

import (
	"github.com/gin-gonic/gin"
	swaggoFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/go-yp/go-openapi-crud-example/apidocs"
	"gitlab.com/go-yp/go-openapi-crud-example/components/api/handler"
	"gitlab.com/go-yp/go-openapi-crud-example/components/api/middleware"
	"gitlab.com/go-yp/go-openapi-crud-example/components/database"
	"gitlab.com/go-yp/go-openapi-crud-example/components/env"
)

func main() {
	var (
		pgCredentials = env.Must("POSTGRES_DSN")
		httpPort      = env.Must("PORT")
	)

	var pgConnection = database.MustConnection(pgCredentials)
	defer pgConnection.Close()

	var repository = database.MustRepository(pgConnection)
	defer repository.Close()

	var r = gin.Default()

	{
		var (
			controller = handler.NewNoteController(repository)
			group      = r.Group("/api/v1/notes")
		)

		group.Use(middleware.Auth())
		group.GET("", controller.Notes)
		group.GET(":id", controller.NoteGet)
		group.POST("", controller.NoteNew)
		group.POST(":id", controller.NoteUpdate)
	}

	{
		r.GET("/apidocs/*any", ginSwagger.WrapHandler(swaggoFiles.Handler))
	}

	{
		r.StaticFile("/", "./public/index.html")
		r.Static("/assets", "./public/assets")
	}

	r.Run(":" + httpPort)
}
