package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/go-yp/go-openapi-crud-example/components/database"
	"gitlab.com/go-yp/go-openapi-crud-example/storage/dbs"
	"net/http"
	"time"
)

const (
	lazyUserID = 1
)

type NoteController struct {
	repository *database.Repository
}

func NewNoteController(repository *database.Repository) *NoteController {
	return &NoteController{repository: repository}
}

type ErrorResponse struct {
	ErrorMessage string `json:"error_message"`
}

type EmptyResponse struct {
}

type Note struct {
	ID         int32  `json:"id"`
	CompanyURL string `json:"company_url"`
	VacancyURL string `json:"vacancy_url"`
	Comment    string `json:"comment"`
}

type NotesResponse []Note

//
// Notes handler
//
// @Tags Notes
// @Accept json
// @Produce json
// @Description Get all notes
// @Success 200 {object} NotesResponse
// @Failure 500 {object} ErrorResponse
// @Router /api/v1/notes [get]
//
func (c *NoteController) Notes(ctx *gin.Context) {
	notes, err := c.repository.Queries().Notes(ctx, lazyUserID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	var result = make([]Note, len(notes))
	for i, note := range notes {
		result[i] = Note{
			ID:         note.ID,
			CompanyURL: note.CompanyUrl,
			VacancyURL: note.VacancyUrl,
			Comment:    note.Comment,
		}
	}

	ctx.JSON(http.StatusOK, result)
}

//
// NoteGet handler
//
// @Tags Notes
// @Accept json
// @Produce json
// @Description Get one note
// @Param id path string true "Note ID"
// @Success 200 {object} EmptyResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /api/v1/notes/{id} [get]
//
func (c *NoteController) NoteGet(ctx *gin.Context) {
	var uri NoteURI

	err := ctx.ShouldBindUri(&uri)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	note, err := c.repository.Queries().NotesGetByID(ctx, dbs.NotesGetByIDParams{
		ID:     uri.ID,
		UserID: lazyUserID,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, &Note{
		ID:         note.ID,
		CompanyURL: note.CompanyUrl,
		VacancyURL: note.VacancyUrl,
		Comment:    note.Comment,
	})
}

type NoteRequest struct {
	CompanyURL string `json:"company_url"`
	VacancyURL string `json:"vacancy_url"`
	Comment    string `json:"comment"`
}

type NotesNewResponse struct {
	ID int32 `json:"id"`
}

//
// NoteNew handler
//
// @Tags Notes
// @Accept json
// @Produce json
// @Description Create note
// @Param NoteRequest body NoteRequest true "note request"
// @Success 201 {object} NotesNewResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /api/v1/notes [post]
//
func (c *NoteController) NoteNew(ctx *gin.Context) {
	var note NoteRequest
	err := ctx.ShouldBindJSON(&note)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	now := time.Now().UTC().Truncate(time.Second)
	id, err := c.repository.Queries().NotesNew(ctx, dbs.NotesNewParams{
		UserID:     lazyUserID,
		CompanyUrl: note.CompanyURL,
		VacancyUrl: note.VacancyURL,
		Comment:    note.Comment,
		Created:    now,
		Updated:    now,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, &NotesNewResponse{
		ID: id,
	})
}

type NoteURI struct {
	ID int32 `uri:"id" binding:"required"`
}

//
// NoteUpdate handler
//
// @Tags Notes
// @Accept json
// @Produce json
// @Description Update note
// @Param id path string true "Note ID"
// @Param NoteRequest body NoteRequest true "note request"
// @Success 200 {object} EmptyResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /api/v1/notes/{id} [post]
//
func (c *NoteController) NoteUpdate(ctx *gin.Context) {
	var uri NoteURI

	err := ctx.ShouldBindUri(&uri)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	var note NoteRequest
	err = ctx.ShouldBindJSON(&note)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	err = c.repository.Queries().NotesUpdate(ctx, dbs.NotesUpdateParams{
		CompanyUrl: note.CompanyURL,
		VacancyUrl: note.VacancyURL,
		Comment:    note.Comment,
		Updated:    time.Now().UTC().Truncate(time.Second),
		ID:         uri.ID,
		UserID:     lazyUserID,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, &ErrorResponse{
			ErrorMessage: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, &EmptyResponse{})
}
