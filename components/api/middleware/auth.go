package middleware

import (
	"github.com/gin-gonic/gin"
)

func Auth() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// @TODO get user from JWT token from cookies
		// ctx.Set("user_id", 1)

		ctx.Next()
	}
}
