# Makefile
stage := $(or $(YAAWS_STAGE), dev)

include Makefile.$(stage)

dev-env:
	cp .dev.env .env

production-env:
	cp .production.env .env

up:
	docker-compose up -d

down:
	docker-compose down

app:
	docker exec -it yaaws_app sh

go-run:
	docker exec yaaws_app go run main.go

postgres-test-run:
	docker exec yaaws_postgres psql -U yaroslav -d yaaws -c "SELECT VERSION();"

goose-install:
	go get -u github.com/pressly/goose/v3/cmd/goose

swag-install:
	go get -u github.com/swaggo/swag/cmd/swag

# openapi-generator-install:
# 	# https://www.npmjs.com/package/@openapitools/openapi-generator-cli
# 	npm install @openapitools/openapi-generator-cli

MIGRATION_NAME=$(or $(MIGRATION), yaaws_init)
migrate-create:
	mkdir -p ./storage/schema
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) create $(MIGRATION_NAME) sql

migrate-up:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) up
migrate-redo:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) redo
migrate-down:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) down
migrate-reset:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) reset
migrate-status:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) status

generate-dbs:
	docker run --rm -v $(shell pwd):/src -w /src kjconroy/sqlc generate

generate-docs:
	swag init --parseInternal --parseDependency -o apidocs -g main.go

generate-ts-client:
	# https://openapi-generator.tech/docs/installation/
	# openapi-generator-cli generate -i ./apidocs/swagger.yaml --generator-name typescript-fetch -o gen/api
	# npm run generate-ts-client
	docker run --rm \
      -v $(shell pwd):/local openapitools/openapi-generator-cli generate \
      --skip-validate-spec \
      -g typescript-fetch \
      -i /local/apidocs/swagger.yaml \
      -o /local/client/gen/api

client-build:
	npm run esbuild