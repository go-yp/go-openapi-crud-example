require("esbuild").buildSync({
    entryPoints: [
        "./client/notes-app.ts",
    ],
    bundle: true,
    minify: true,
    outdir: "./public/assets/js",
});