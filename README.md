# Go OpenAPI CRUD example

### Development
```bash
make dev-env
make up
make go-run
make down
```

##### Tools
```bash
make goose-install
make swag-install
```

### Generate
```
make generate-dbs
make generate-docs
```

### Open
```bash
browse http://localhost:8080/apidocs/index.html
```