-- +goose Up
-- +goose StatementBegin
CREATE TABLE users
(
    id       SERIAL PRIMARY KEY,
    username VARCHAR(50) UNIQUE       NOT NULL,
    created  TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE notes
(
    id          SERIAL PRIMARY KEY,
    user_id     INT                      NOT NULL REFERENCES users (id),
    company_url VARCHAR                  NOT NULL,
    vacancy_url VARCHAR                  NOT NULL,
    comment     VARCHAR                  NOT NULL,
    created     TIMESTAMP WITH TIME ZONE NOT NULL,
    updated     TIMESTAMP WITH TIME ZONE NOT NULL
);

INSERT INTO users (id, username, created)
VALUES (1, 'go-developer', NOW());
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS notes;
DROP TABLE IF EXISTS users;
-- +goose StatementEnd
