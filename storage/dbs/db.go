// Code generated by sqlc. DO NOT EDIT.

package dbs

import (
	"context"
	"database/sql"
	"fmt"
)

type DBTX interface {
	ExecContext(context.Context, string, ...interface{}) (sql.Result, error)
	PrepareContext(context.Context, string) (*sql.Stmt, error)
	QueryContext(context.Context, string, ...interface{}) (*sql.Rows, error)
	QueryRowContext(context.Context, string, ...interface{}) *sql.Row
}

func New(db DBTX) *Queries {
	return &Queries{db: db}
}

func Prepare(ctx context.Context, db DBTX) (*Queries, error) {
	q := Queries{db: db}
	var err error
	if q.notesStmt, err = db.PrepareContext(ctx, notes); err != nil {
		return nil, fmt.Errorf("error preparing query Notes: %w", err)
	}
	if q.notesGetByIDStmt, err = db.PrepareContext(ctx, notesGetByID); err != nil {
		return nil, fmt.Errorf("error preparing query NotesGetByID: %w", err)
	}
	if q.notesNewStmt, err = db.PrepareContext(ctx, notesNew); err != nil {
		return nil, fmt.Errorf("error preparing query NotesNew: %w", err)
	}
	if q.notesUpdateStmt, err = db.PrepareContext(ctx, notesUpdate); err != nil {
		return nil, fmt.Errorf("error preparing query NotesUpdate: %w", err)
	}
	return &q, nil
}

func (q *Queries) Close() error {
	var err error
	if q.notesStmt != nil {
		if cerr := q.notesStmt.Close(); cerr != nil {
			err = fmt.Errorf("error closing notesStmt: %w", cerr)
		}
	}
	if q.notesGetByIDStmt != nil {
		if cerr := q.notesGetByIDStmt.Close(); cerr != nil {
			err = fmt.Errorf("error closing notesGetByIDStmt: %w", cerr)
		}
	}
	if q.notesNewStmt != nil {
		if cerr := q.notesNewStmt.Close(); cerr != nil {
			err = fmt.Errorf("error closing notesNewStmt: %w", cerr)
		}
	}
	if q.notesUpdateStmt != nil {
		if cerr := q.notesUpdateStmt.Close(); cerr != nil {
			err = fmt.Errorf("error closing notesUpdateStmt: %w", cerr)
		}
	}
	return err
}

func (q *Queries) exec(ctx context.Context, stmt *sql.Stmt, query string, args ...interface{}) (sql.Result, error) {
	switch {
	case stmt != nil && q.tx != nil:
		return q.tx.StmtContext(ctx, stmt).ExecContext(ctx, args...)
	case stmt != nil:
		return stmt.ExecContext(ctx, args...)
	default:
		return q.db.ExecContext(ctx, query, args...)
	}
}

func (q *Queries) query(ctx context.Context, stmt *sql.Stmt, query string, args ...interface{}) (*sql.Rows, error) {
	switch {
	case stmt != nil && q.tx != nil:
		return q.tx.StmtContext(ctx, stmt).QueryContext(ctx, args...)
	case stmt != nil:
		return stmt.QueryContext(ctx, args...)
	default:
		return q.db.QueryContext(ctx, query, args...)
	}
}

func (q *Queries) queryRow(ctx context.Context, stmt *sql.Stmt, query string, args ...interface{}) *sql.Row {
	switch {
	case stmt != nil && q.tx != nil:
		return q.tx.StmtContext(ctx, stmt).QueryRowContext(ctx, args...)
	case stmt != nil:
		return stmt.QueryRowContext(ctx, args...)
	default:
		return q.db.QueryRowContext(ctx, query, args...)
	}
}

type Queries struct {
	db               DBTX
	tx               *sql.Tx
	notesStmt        *sql.Stmt
	notesGetByIDStmt *sql.Stmt
	notesNewStmt     *sql.Stmt
	notesUpdateStmt  *sql.Stmt
}

func (q *Queries) WithTx(tx *sql.Tx) *Queries {
	return &Queries{
		db:               tx,
		tx:               tx,
		notesStmt:        q.notesStmt,
		notesGetByIDStmt: q.notesGetByIDStmt,
		notesNewStmt:     q.notesNewStmt,
		notesUpdateStmt:  q.notesUpdateStmt,
	}
}
