-- name: NotesNew :one
INSERT INTO notes (user_id, company_url, vacancy_url, comment, created, updated)
VALUES (@user_id, @company_url, @vacancy_url, @comment, @created, @updated)
RETURNING id;

-- name: Notes :many
SELECT *
FROM notes
WHERE user_id = @user_id
ORDER BY created DESC;

-- name: NotesGetByID :one
SELECT *
FROM notes
WHERE id = @id
  AND user_id = @user_id;

-- name: NotesUpdate :exec
UPDATE notes
SET company_url = @company_url,
    vacancy_url = @vacancy_url,
    comment     = @comment,
    updated     = @updated
WHERE id = @id
  AND user_id = @user_id;