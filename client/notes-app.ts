import {
    ApiV1NotesPostRequest,
    Configuration,
    HandlerNote,
    HandlerNoteRequest,
    HandlerNotesNewResponse,
    NotesApi
} from "./gen/api";

console.log("Notes app");

const notesApiClient = new NotesApi(new Configuration({
    basePath: "http://localhost:8080",
}));

notesApiClient.apiV1NotesGet()
    .then(function (notes: HandlerNote[]) {
        console.log(notes);
        console.log(JSON.stringify(notes));
        document.getElementById("notes").innerHTML = JSON.stringify(notes);
    })
    .catch(console.error);

const now = Date.now();

notesApiClient.apiV1NotesPost({
    noteRequest: {
        companyUrl: `https://example.com/company/${now}`,
        vacancyUrl: `https://example.com/vacancy/${now}`,
        comment: `some comment ${now}`,
    },
})
    .then(function (response: HandlerNotesNewResponse) {
        console.log(response);
        console.log(JSON.stringify(response));
    })
    .catch(console.error);